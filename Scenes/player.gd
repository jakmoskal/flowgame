extends RigidBody2D

const DIAMOND = preload("res://Diamonds.tscn")

var points_earned = 0
var parameters = {'horizontal_speed': 10, 'air_friction' : 0.5}

var x = 0
var accelerate = 0
var input_states = preload("res://input_states.gd")

var movement_enabled = true
var state_left  = input_states.new("ui_left")
var state_right = input_states.new("ui_right")
var vertical_speed = 50

var btn_left    = null
var btn_right   = null


var prev_y_position = 0

func _ready():
	set_bounce(0.5)
	set_process_input(true)

func _process(delta):
	process_input( delta )
	linear_velocity *= 0.97
	$Sprite.rotation = -linear_velocity.x/900 if linear_velocity.y > 0 else linear_velocity.x/900
	$Sprite.flip_h = (linear_velocity.x < 0 and linear_velocity.y < 0) or (linear_velocity.x > 0 and linear_velocity.y > 0)
	$Sprite.flip_v = linear_velocity.y > 0
	apply_impulse(Vector2(), Vector2(accelerate, -vertical_speed))
	get_node("Sprite/retryButton").disabled = true
	checkGroundCollision()
	
	if(get_node("Sprite/retryButton").pressed == true):
		get_tree().paused = false
		print("HELLO")
		get_tree().reload_current_scene()
		
	
	if(int(rand_range(1,100)) > 95) :
		generateDiamond()
	
	vertical_speed -= 0.1
		
	if(position.y < 100) :
		vertical_speed -= 0.05
	else :
		vertical_speed = 0
		
	
	update()
	
func _input(event):
	accelerate = 0
	
func process_input( delta ):
	btn_left  = state_left.check()
	btn_right = state_right.check()
	accelerate = 0
	
	if(movement_enabled == true):
		if btn_left  > 1:
			accelerate = -parameters['horizontal_speed']
		if btn_right > 1:
			accelerate = parameters['horizontal_speed']
		
func _draw():
#	draw_line(Vector2(), Vector2(linear_velocity), Color(1,0,0), 3 )
	pass

func generateDiamond():
	 var diamond = DIAMOND.instance()
	 diamond.global_position.y = global_position.y - 1500
	 diamond.global_position.x = global_position.x - 1000 + rand_range(1,1000)
	 get_parent().add_child(diamond)
	
func checkGroundCollision():
	if position.y>221:
		movement_enabled = false
		get_node("Label").set_text("YOUR NUMBER OF POINTS : " + str(points_earned) + "!" + "\nCLICK ON THE SHEEP TO TRY AGAIN!")
		get_tree().paused = true
		get_node("Sprite/retryButton").disabled = false